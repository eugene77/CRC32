# Efficient CRC32 implementation in C#

This is the result of my investigation in implementing CRC32 algorithm using managed C# code only.

The most popular CRC32 implementation is naive byte-by-byte processing with lookup table.

It can be accelerated by accessing memory in 32-bit words. This simple improvement boosts performance significantly, especially on 64-bit platform. I found efficient implementation in assembler in [7-zip project sources](https://www.7-zip.org/sdk.html) and implemented similar one in C# in this project, see `OptimizedCRC` class. 

Further acceleration can be achieved on multiprocessor systems with multithreaded optimization. Idea is simple: split original string into two (or more) parts and calculate CRC value for each part independently. Then CRCs of all parts combined to make CRC value of the whole string. The combining algorithm is based on Galois fields algebra. I found it in [DotNetZip project](http://dotnetzip.codeplex.com) sources. Note that this works only for large strings (hundreds of kilobytes and more); for smaller data performance win may be overridden by threads managing overhead. See `ParallelCRC` class.

This implementation serves as proof-of-concept only. In particular, it does not implement `HashAlgorithm`
class.

`OptimizedCRC` class is published under the MIT License. 

`ParallelCRC` class is published under Microsoft Public License since it contains code from DotNetZip sources published under MS-PL.

![screenshot2.png](screenshot2.png)
