﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace CRC32
{
    class Program
    {
        static void Main(string[] args)
        {
            var rnd = new Random();

            Console.WriteLine();
            Console.WriteLine("Platform:       " + (IntPtr.Size * 8) + "bit");
            Console.WriteLine("ProcessorCount: " + Environment.ProcessorCount);
            Console.WriteLine();

            int dataSize = (int)Math.Min(Environment.ProcessorCount * 10L * 1048576, 1 << 30);
            Console.WriteLine(string.Format("preparing {0}Mb data array...", dataSize >> 20));
            var data = new byte[dataSize];
            rnd.NextBytes(data);

            var implementations = new[]
            {
                new ImplementationTest("Naive implementation:         ", NaiveCRC.Compute),
                new ImplementationTest("Optimized implementation:     ", OptimizedCRC.Compute),
                new ImplementationTest("Multithreaded implementation: ", ParallelCRC.Compute)
            };

            while (true)
            {
                var count = rnd.Next(data.Length + 1);
                var offset = rnd.Next(data.Length - count + 1);


                // pick 2 implementations, run them and check if they give the same result

                foreach (var i in implementations) i.Key = rnd.Next();
                var chosen = implementations.OrderBy(i => i.Key).Take(2).ToArray();

                var crc1 = chosen[0].Run(data, offset, count);
                var crc2 = chosen[1].Run(data, offset, count);

                if (crc1 != crc2) throw new Exception("CRC values mismatch");


                // display performance stats

                Console.SetCursorPosition(0, 4);
                foreach (var i in implementations)
                {
                    Console.WriteLine(i.ToString().PadRight(60));
                }
            }
        }

        class ImplementationTest
        {
            private string Name;
            private long TotalProcessedCount;
            private Stopwatch Stopwatch = new Stopwatch();
            private Func<byte[], int, int, int> Implementation;

            public int Key;

            public ImplementationTest(string name, Func<byte[], int, int, int> implementation)
            {
                Implementation = implementation;
                Name = name;
            }

            public int Run(byte[] data, int offset, int count)
            {
                Stopwatch.Start();
                var crc = Implementation(data, offset, count);
                Stopwatch.Stop();
                TotalProcessedCount += count;
                return crc;
            }

            public override string ToString()
            {
                double time = Stopwatch.ElapsedMilliseconds;
                if (time < 10) return "---";
                return Name + (long)(TotalProcessedCount / time) + " KB/s";
            }
        }

    }
}
